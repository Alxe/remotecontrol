import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import es.uca.almendro.remotecontrol.api.Procedure.ProcedureType;
import es.uca.almendro.remotecontrol.api.Request;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.client.Client;
import es.uca.almendro.remotecontrol.exception.RCException;
import es.uca.almendro.remotecontrol.server.PropertyLoader;
import es.uca.almendro.remotecontrol.server.Server;

public class LaunchServer {
	private static final int PORT = 16352;
	
	private static final Request REQUEST = 
			new Request(ProcedureType.GET_FILES, 
					new String[0]);


	private static Server serv = null;
	
	public static void main(String[] args) throws Exception {
		
		Thread tServer = new Thread(new Runnable() {

			public void run() {
				Properties serverProperties = new Properties(); 
				
				File f = new File("server.properties");
				
				try {
					if(!f.exists()) f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				serverProperties = PropertyLoader.loadProperties(f);
				
				Server server = serv 
						= new Server(serverProperties);
				
				try {
					server.deploy(PORT);
				} catch (RCException e) {
					server.close();
				}
			}
			
		});
		
		Thread tClient = new Thread(new Runnable() {

			public void run() {
				Client client = new Client();
				try {
					client.connect(InetAddress.getByName("localhost"), PORT);
				} catch (RCException e) {
					e.printStackTrace();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				
				System.out.println(client.isConnected());
				
				
				try {
					Response<?> RESPONSE = client.send(REQUEST);
					
					System.out.println(REQUEST);
					System.out.println(RESPONSE);
				} catch (RCException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		System.out.println(System.getProperty("os.name"));
		
		tServer.start();
		
		Thread.sleep(1000);
		
		tClient.start();
		
		Thread.sleep(1000);
		
		serv.close();
		tServer.interrupt();
		tServer.join();
	}
}
