package es.uca.almendro.remotecontrol.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Properties;

import es.uca.almendro.remotecontrol.api.Request;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.exception.RCException;
import es.uca.almendro.remotecontrol.utils.RC;

public class Server {
	
	private final Properties properties;
	
	private ServerSocket server = null;
	
	private boolean closed = false;
	
	public Server(Properties properties) {
		this.properties = new Properties(properties);
	}
	
	public void deploy(int port) throws RCException {
		try {
			server = new ServerSocket(port);
			
			closed = false;
			
			while(!closed) {
				try {
					connect(server.accept());
					
				} catch(SocketException e) {
					// Expected; do nothing.
					// Produced on ServerSocket#close() when ServerSocket#accept() blocks.
				} catch(IOException e) {
					RC.LOGGER.error(getClass().getSimpleName(),
							"An error has occured during IO operations", e);
				}
			}
		} catch(IOException e) {
			final String message = "Could not deploy server to port " + port;
			
			RC.LOGGER.rethrow(getClass().getSimpleName(), 
					message, new RCException(message, e));
		}
	}
	
	private void connect(Socket connection) throws RCException {
		try {
			ObjectInputStream ois = 
					new ObjectInputStream(connection.getInputStream());
			
			Response<?> response = null;
			
			try {
				response = recieve((Request) ois.readObject());
			} catch (ClassNotFoundException e) {
				response = RC.response.GENERIC_ERROR;
			}
			
			ObjectOutputStream oos = 
					new ObjectOutputStream(connection.getOutputStream());
			
			oos.writeObject(response);
		} catch(IOException e) {
			RC.LOGGER.rethrow(getClass().getSimpleName(),
					"Error during connection", new RCException(e));
		}
	}
	
	private Response<?> recieve(Request request) {
		return Controller.executeRequestedProcedure(request, properties);
	}
	
	public void close() {
		if(server != null && !server.isClosed()) {
			try {
				closed = true; server.close();
				
			} catch (IOException e) {
				RC.LOGGER.error(getClass().getSimpleName(),
						"Error during server closing", e);
			}
		}
	}
}
