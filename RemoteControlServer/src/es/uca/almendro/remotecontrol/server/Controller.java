package es.uca.almendro.remotecontrol.server;

import java.util.Properties;

import es.uca.almendro.remotecontrol.api.Procedure.ProcedureType;
import es.uca.almendro.remotecontrol.api.Request;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.api.Response.ResponseType;

/**
 * Controller class that re-routes the request to a procedure.
 * @author Alex
 *
 */
public class Controller {

	/**
	 * Executes a procedure given a request, or none if it doesn't exist, and returns a response.
	 * @param request The request from the client
	 * @param serverProperties The properties of the server which executes this.
	 * @return
	 */
	public static Response<?> executeRequestedProcedure(Request request, Properties serverProperties) {
		try {
			ProcedureType procType = ProcedureType.valueOf(request.getProcName());
			
			return procType.getInstance()
					.execute(request.getArgs(), serverProperties);
			
		} catch(InstantiationException e) {
			// An error occured during instantiation
			throw new IllegalArgumentException(e);
			
		} catch(IllegalArgumentException e) {
			// ProcedureType doesn't exist
			return new Response<Void>(ResponseType.ERROR,
					"Requested procedure doesn't exist", new Void[0]);
		}
	}

}
