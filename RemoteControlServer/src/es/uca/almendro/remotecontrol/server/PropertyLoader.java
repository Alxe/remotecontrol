package es.uca.almendro.remotecontrol.server;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import es.uca.almendro.remotecontrol.utils.RC;

public class PropertyLoader {
	
	public static Properties loadProperties(File file) {
		Properties properties = new Properties();

		try {
			Reader fileReader = null;
			
			// Try to find file
			try {
				fileReader = new FileReader(
						file);
			} catch (IOException e) {
				RC.LOGGER.error(PropertyLoader.class.getSimpleName(),
						"File could not be found, aborting load", null);

				throw e;
			}

			// Try to parse file
			try {
				properties.load(fileReader);
			} catch (IOException e) {
				RC.LOGGER.error(PropertyLoader.class.getSimpleName(),
						"Error while reading file, aborting load", null);

				throw e;
			}

		} catch (IOException e) {
			RC.LOGGER.error(
					PropertyLoader.class.getSimpleName(),
					"Unable to read properties from file. Blank properties will be returned",
					e);
		}		
		
		return properties;
	}
}
