package es.uca.almendro.remotecontrol.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import es.uca.almendro.remotecontrol.api.Request;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.exception.RCException;
import es.uca.almendro.remotecontrol.utils.RC;

/**
 * Client which connects to a Server via Sockets. May send requests.
 * @author Alex
 *
 */
public class Client {
	/**
	 * The socket that represents the connection between client and server.
	 */
	private Socket connection = null;
	
	/**
	 * Connects to a given IP address and port.
	 * @param address The abstracted IP address. May be either IPv4 or IPv6.
	 * @param port The port which will communicate with.
	 * @throws IOException If an IOException occurs during execution.
	 */
	public void connect(InetAddress address, int port) throws RCException {
		if(isConnected()) {
			try {
				// If connected, try to close current connection.
				this.connection.close();
				
			} catch (IOException e) {
				final String message = "Error during socket connection closing";
				
				RC.LOGGER.rethrow(getClass().getSimpleName(),
						message, new RCException(message, e));
			}
		}
		
		
		try {
			this.connection = new Socket(address, port);
			
		} catch (IOException e) {
			final String message = "Error during connection to server at "
						+ address + " : " + port;
			
			RC.LOGGER.rethrow(getClass().getSimpleName(), 
					message, new RCException(message, e));
		}
	}
	
	/**
	 * Sends a {@link Request} and returns a {@link Response}.
	 * @param request The sent request object.
	 * @return The received response object. It's type is dependent on the request.
	 * @throws IOException If a IOException was thrown during execution.
	 */
	public Response<?> send(Request request) throws RCException {
		Response<?> response = RC.response.GENERIC_ERROR;
		
		if(!isConnected()) throw new RCException("client is closed");
		
		// Writing block
		try {
			// Wrap OutputStream in order to write the request.
			ObjectOutputStream oos = 
					new ObjectOutputStream(connection.getOutputStream());
			
			// Write the request as an Object.
			oos.writeObject(request);
			
		} catch(IOException e) {
			final String message = "Error while writing to server";
			
			RC.LOGGER.rethrow(getClass().getSimpleName(),
					message, new RCException(message, e));
		}
		
		// Reading block
		try {
			// Wrap InputStream in order to read the response.
			ObjectInputStream ois =
					new ObjectInputStream(connection.getInputStream());
			
			// Read the object as a Response.
			response = (Response<?>) ois.readObject();
			
		} catch(IOException e) {
			final String message = "Error while reading from server";
			
			RC.LOGGER.rethrow(getClass().getSimpleName(),
					message, new RCException(message, e));
			
		} catch (ClassNotFoundException e) {
			final String message = "Unexpected object recieved from server";
			
			RC.LOGGER.rethrow(getClass().getSimpleName(),
					message, new RCException(message, e));
			
		}
		
		// Return the response: either an error or the related response.
		return response;
	}
	
	/**
	 * Checks if client is currently connected (socket is alive and connected)
	 * @return True if connected, false if not.
	 */
	public boolean isConnected() {
		return (connection != null)
					&& connection.isConnected() 
					&& !connection.isClosed();
	}
}
