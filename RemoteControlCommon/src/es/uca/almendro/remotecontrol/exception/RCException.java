package es.uca.almendro.remotecontrol.exception;

/**
 * Wrapper {@link Exception} for usage in this framework
 * @author Alex
 *
 */
public class RCException extends Exception {

	private static final long serialVersionUID = 2653124461013342370L;

	public RCException(String string) {
		super(string);
	}
	
	public RCException(Throwable throwable) {
		super(throwable);
	}
	
	public RCException(String string, Throwable throwable) {
		super(string, throwable);
	}

}
