package es.uca.almendro.remotecontrol.api;

import java.util.Properties;

import es.uca.almendro.remotecontrol.api.procedures.EchoProcedure;
import es.uca.almendro.remotecontrol.api.procedures.GetFilesProcedure;
import es.uca.almendro.remotecontrol.api.procedures.OpenFileProcedure;
import es.uca.almendro.remotecontrol.utils.RC;

/**
 * Class that represents a mapping from action requested to OS-native functions
 * @author Alex
 *
 */
public abstract class Procedure {
	
	/**
	 * Enumeration of types of procedures, mapped to the class it delegates to
	 * @author Alex
	 *
	 */
	public static enum ProcedureType {
		ECHO(EchoProcedure.class),
		GET_FILES(GetFilesProcedure.class), 
		OPEN_FILES(OpenFileProcedure.class);
		
		/**
		 * Contained Procedure class
		 */
		private final Class<? extends Procedure> clazz;
		
		private Procedure instance = null;
		
		private ProcedureType(Class<? extends Procedure> clazz) {
			this.clazz = RC.objects.requireNonNull(clazz);
		}
		
		/** 
		 * Returns the corresponding Procedure object, instantiates it if not found on cache
		 * @return The Procedure object
		 * @throws InstantiationException Error during instantiation
		 */
		public Procedure getInstance() throws InstantiationException {
			if(instance != null) {
				return instance;
			}
			
			try {
				return (instance = clazz.newInstance());
			} catch (IllegalAccessException e) {
				throw new InstantiationException(e.getMessage());
			}
		}
	}
	
	protected Procedure() {
		
	}
	
	/**
	 * Executes the procedure and delivers a response.
	 * @param args Procedure arguments (like file to execute)
	 * @param serverProperties Properties from the server in which it is executed.
	 * @return
	 */
	public abstract Response<?> execute(String[] args, Properties serverProperties);
}
