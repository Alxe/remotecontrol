package es.uca.almendro.remotecontrol.api;

import java.io.Serializable;
import java.util.Arrays;

import es.uca.almendro.remotecontrol.utils.RC;

/**
 * Abstract representation of a response sent from the server as the result of some procedure execution.
 * @author Alex
 * @param <T> The type of the contained data.
 */
public class Response<T> implements Serializable {
	private static final long serialVersionUID = 1933067483024338089L;

	/**
	 * Enumeration of the Response types.
	 * @author Alex
	 *
	 */
	public static enum ResponseType {
		OKAY,
		OKAY_NO_DATA,
		ERROR;
	}
	
	/**
	 * The resulting state of the response.
	 */
	private final String response;
	
	/**
	 * An optional description to the response (for error reporting)
	 */
	private final String description;
	
	/**
	 * The resulting data from the procedure execution
	 */
	private final T[] data;
	
	/**
	 * Creates a new Response
	 * @param responseType Enumerated representation of the response. Non null.
	 * @param description Additional description of the response. Non null.
	 * @param data The data returned from the procedure execution. Non null.
	 */
	public Response(ResponseType responseType, String description, T[] data) {
		this(responseType.name(), description, data);
	}
	
	/**
	 * Creates a new Response
	 * @param response String representation of the response. Non null.
	 * @param description Additional description of the response. Non null.
	 * @param data The data returned from the procedure execution. Non null.
	 */
	public Response(String response, String description, T[] data) {
		this.response = RC.objects.requireNonNull(response);
		this.description = RC.objects.requireNonNull(description);
		this.data = Arrays.copyOf(
				RC.objects.requireNonNull(data), data.length);
	}
	
	/**
	 * Retrieves the response of the procedure.
	 * @return The response of the procedure.
	 */
	public String getResponse() {
		return response;
	}
	
	/**
	 * Retrieves the description given by the procedure.
	 * @return The description given by the procedure.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Retrieves a copy of the data returned from the procedure.
	 * @return A copy of the data returned from the procedure.
	 */
	public T[] getData() {
		return Arrays.copyOf(data, data.length);
	}
	
	/**
	 * Checks if the returned data is existent
	 * @return True if there is data, false otherwise.
	 */
	public boolean hasData() {
		return (data.length > 0);
	}
	
	@Override
	public String toString() {
		return RC.GSON.toJson(this);
	}
}
