package es.uca.almendro.remotecontrol.api;

import java.io.Serializable;
import java.util.Arrays;

import es.uca.almendro.remotecontrol.api.Procedure.ProcedureType;
import es.uca.almendro.remotecontrol.utils.RC;

/**
 * Abstract representation of a request sent to the server in order to execute some procedure.
 * @author Alex
 *
 */
public class Request implements Serializable {
	private static final long serialVersionUID = 687681711692860758L;

	/**
	 * The name of the Procedure to execute.
	 */
	private final String procName;

	/**
	 * The arguments sent along to modify the procedure.
	 */
	private final String[] args;
	
	/**
	 * Constructs a new Request
	 * @param procType Enumerated representation of the procedure. Non null.
	 * @param args Arguments sent along for the procedure. Non null.
	 */
	public Request(ProcedureType procType, String[] args) {
		this(procType.name(), args);
	}

	/**
	 * Constructs a new Request
	 * @param procName String representation of the procedure. Non null.
	 * @param args Arguments sent along for the procedure. Non null.
	 */
	public Request(String procName, String[] args) {
		this.procName = RC.objects.requireNonNull(procName);
		this.args = Arrays.copyOf(
				RC.objects.requireNonNull(args), args.length);
	}

	/**
	 * Retrieves the procedure name.
	 * @return The procedure name
	 */
	public String getProcName() {
		return procName;
	}

	/**
	 * Retrieves a copy of the argument array.
	 * @return A copy of the argument array.
	 */
	public String[] getArgs() {
		return Arrays.copyOf(args, args.length);
	}
	
	/**
	 * Checks if it has arguments.
	 * @return True if arguments are present, false otherwise.
	 */
	public boolean hasArgs() {
		return (args.length > 0);
	}

	@Override
	public String toString() {
		return RC.GSON.toJson(this);
	}
}
