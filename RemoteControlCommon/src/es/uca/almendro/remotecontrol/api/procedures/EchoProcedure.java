package es.uca.almendro.remotecontrol.api.procedures;

import java.util.Properties;

import es.uca.almendro.remotecontrol.api.Procedure;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.utils.RC;

/**
 * Simple test-like procedure that returns the recieved data, <em>echoing</em> it.
 * @author Alex
 *
 */
public class EchoProcedure extends Procedure {

	@Override
	public Response<?> execute(String[] args, Properties serverProperties) {
		return RC.response
				.okay("Echoed correctly!", args);
	}

}
