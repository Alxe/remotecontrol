package es.uca.almendro.remotecontrol.api.procedures;

import java.util.Properties;

import es.uca.almendro.remotecontrol.api.Procedure;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.utils.RC;

public class OpenFileProcedure extends Procedure {

	@Override
	public Response<?> execute(String[] args, Properties serverProperties) {
		if(args.length > 0) {
			
			// TODO Do something?
			
			return RC.response
					.datalessOkay("File opened or should be opening");
		} else {
			return RC.response
					.datalessError("No file name specified");
		}
	}

}
