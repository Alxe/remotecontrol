package es.uca.almendro.remotecontrol.api.procedures;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Properties;

import es.uca.almendro.remotecontrol.api.Procedure;
import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.utils.RC;

/**
 * Retrieves all the filed broadcasted by the server that pass a type check, defined in the server properties.
 * @author Alex
 *
 */
public class GetFilesProcedure extends Procedure {
	/**
	 * A filename filter that checks for the extension/type of the file, given an array of String, dot-based extension/type.
	 * @author Alex
	 *
	 */
	private static class TypeFilter implements FilenameFilter {
		
		/**
		 * Convenience method to check if a TypeFilter needs updating.
		 * @param typeFilter The TypeFilter that needs updating. May be null.
		 * @param serverProperties The properties of the server on which this is executed.
		 * @return A new (if provided was null) or updated TypeFilter.
		 */
		public static final TypeFilter updateTypeFilter(TypeFilter typeFilter, Properties serverProperties) {
			String[] newTypes = TypeFilter.getTypesFromProperties(serverProperties);

			if (typeFilter != null) {
				String[] oldTypes = typeFilter.getTypes();

				if (!Arrays.equals(oldTypes, newTypes)) {
					typeFilter = new TypeFilter(newTypes);
				}

			} else {

				typeFilter = new TypeFilter(newTypes);
			}

			return typeFilter;
		}
		
		/**
		 * Helper method to check if TypeFilter needs updating.
		 * @param serverProperties The properties of the server on which this is executed.
		 * @return An array of String, dot-based extension/type.
		 */
		private static final String[] getTypesFromProperties(Properties serverProperties) {
			return serverProperties.getProperty(
					RC.properties.BROADCAST_ALLOWED_TYPES,
					RC.properties.defaults.BROADCAST_ALLOWED_TYPES).split(",");
		}

		/**
		 * String array of the supported types
		 */
		private final String[] types;

		/**
		 * Constructs a new TypeFilter
		 * @param types An array of String, dot-based extension/type. Non null.
		 */
		public TypeFilter(String[] types) {
			this.types = Arrays.copyOf(
					RC.objects.requireNonNull(types), types.length);
		}

		@Override
		public boolean accept(File dir, String name) {
			for (int i = 0; i < types.length; i++) {
				final String type = types[i];
				
				if (!type.isEmpty() &&
						name.toLowerCase().endsWith(
								type.toLowerCase())) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Retrieves a copy of the types this TypeFilter checks for.
		 * @return A copy of the types this TypeFilter checks for.
		 */
		public String[] getTypes() {
			return Arrays.copyOf(types, types.length);
		}
	}

	/**
	 * The TypeFilter used to filter files.
	 */
	private TypeFilter typeFilter = null;

	@Override
	public Response<?> execute(String[] args, Properties serverProperties) {
		// As a default, return a generic error.
		Response<?> result = RC.response.GENERIC_ERROR;

		// Checks if typeFilter needs updating and updates if needed.
		this.typeFilter = TypeFilter.updateTypeFilter(typeFilter, serverProperties);

		// Delegates the execution to the Operating System.
		if (RC.os.isWindows()) {
			result = getFilesWin(args, serverProperties);
		}

		return result;
	}

	private final Response<?> getFilesWin(String[] args,
			Properties serverProperties) {

		File broadcast = new File(
				serverProperties.getProperty(
						RC.properties.BROADCAST_PATH),
						RC.properties.defaults.BROADCAST_PATH);

		if (broadcast.isDirectory()) {
			File[] files = broadcast.listFiles(typeFilter);

			String[] filenames = new String[files.length];

			for (int i = 0; i < files.length; i++) {
				filenames[i] = files[i].getName();
			}

			if(filenames.length > 0) {
				return RC.response
					.okay("Files broadcasted by server", filenames);
			}
		}

		return RC.response
				.datalessError("Server did not broadcast any files");
	}

}
