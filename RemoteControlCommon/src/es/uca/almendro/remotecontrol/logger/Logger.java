package es.uca.almendro.remotecontrol.logger;

/**
 * Interface that defines the contract methods for a given logger.
 * @author Alex
 *
 */
public interface Logger {
	/**
	 * Logs a message as information (non-error info)
	 * @param tag The tag that will name the message
	 * @param message The message to be logged
	 */
	public void info(String tag, String message);
	
	/**
	 * Logs a message as an error
	 * @param tag The tag that will name the message
	 * @param message The message to be logged
	 * @param exception The accompanying exception. May be null
	 */
	public void error(String tag, String message, Exception exception);
	
	/**
	 * Logs a message as an error and rethrows a given exception
	 * @param tag The tag that will name the message
	 * @param message The message to be logged
	 * @param exception The accompanying exception. May be null
	 */
	public <T extends Exception> void rethrow(String tag, String message, T exception) throws T;
}
