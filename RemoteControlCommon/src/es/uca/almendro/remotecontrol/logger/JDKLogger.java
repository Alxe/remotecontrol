package es.uca.almendro.remotecontrol.logger;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * JDK (and probably standard) implementation of the {@link Logger} interface.
 * @author Alex
 *
 */
public class JDKLogger implements Logger {
	
	/**
	 * Pattern of the output.
	 */
	private static final String PATTERN = "[%1$s] %2$s: %3$s.\n";
	
	/**
	 * Date formatter
	 */
	private static final SimpleDateFormat DATE_FORMATTER
		= new SimpleDateFormat("HH:mm:ss");

	@Override
	public void info(String tag, String message) {
		System.out.printf(PATTERN, new Object[] {
			DATE_FORMATTER.format(new Date()), tag, message
		});
	}

	@Override
	public void error(String tag, String message, Exception exception) {
		System.err.printf(PATTERN, new Object[] {
			DATE_FORMATTER.format(new Date()), tag, message
		});
		
		if(exception != null) exception.printStackTrace(System.err);
	}
	
	public <T extends Exception> void rethrow(String tag, String message, T exception) throws T {
		this.error(tag, message, exception); 
		
		if(exception != null) throw exception;
	}
}
