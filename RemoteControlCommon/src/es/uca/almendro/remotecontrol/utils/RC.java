package es.uca.almendro.remotecontrol.utils;

import com.google.gson.Gson;

import es.uca.almendro.remotecontrol.api.Response;
import es.uca.almendro.remotecontrol.api.Response.ResponseType;
import es.uca.almendro.remotecontrol.logger.JDKLogger;
import es.uca.almendro.remotecontrol.logger.Logger;

/**
 * Global state and methods, namely utility methods and constants.
 * @author Alex
 *
 */
public final class RC {

	/**
	 * Constants or utility methods for the Operating System.
	 * @author Alex
	 *
	 */
	public static final class os {
		private static String OS_NAME;
		
		/**
		 * Retrieve the current OS name as a String.
		 * @return The current OS name as a String.
		 */
		public static String getOSName() {
			if(OS_NAME == null) OS_NAME = System.getProperty("os.name");
			
			return OS_NAME;
		}
		
		/**
		 * Checks if the running OS is Windows-based.
		 * @return True if Windows, false otherwise.
		 */
		public static boolean isWindows() {
			return getOSName().toLowerCase()
					.startsWith("win");
		}
	
		private os() {}
	}
	
	/**
	 * Constants or utility methods for Arrays.
	 * @author Alex
	 *
	 */
	public static final class array {
		/**
		 * Empty, convenience for Generic treatment, Void array of zero length.
		 */
		public static final Void[] EMPTY_VOID_ARRAY = new Void[0];
	
		private array() {}
	}
	
	/**
	 * Constants or utility methods for Responses.
	 * @author Alex
	 *
	 */
	public static final class response {
		/**
		 * Generic, data-less error.
		 */
		public static final Response<?> GENERIC_ERROR = 
				datalessError("An error has occurred");
		
		/**
		 * Creates an OKAY response with a given description and data array.
		 * @param description The description of the response.
		 * @param data The data of the response.
		 * @return A response object that meets the given parameters.
		 */
		public static final <T> Response<?> okay(String description, T[] data) {
			if(data == null || data.length <= 0) {
				return datalessOkay(description);
			}
			
			return new Response<T>(ResponseType.OKAY, description, data);
		}
		
		/**
		 * Creates an OKAY_NO_DATA response with a given description.
		 * @param description The description of the response.
		 * @return A response object that meets the given parameters.
		 */
		public static final Response<?> datalessOkay(String description) {
			return new Response<Void>(ResponseType.OKAY_NO_DATA,
					description, RC.array.EMPTY_VOID_ARRAY);
		}
		
		/**
		 * Creates an ERROR response with a given description with no data.
		 * @param description The description of the response.
		 * @return A response object that meets the given parameters.
		 */
		public static final Response<?> datalessError(String description) {
			return new Response<Void>(ResponseType.ERROR, description, RC.array.EMPTY_VOID_ARRAY);
		}
	
		private response() {}
	}
	
	/**
	 * Constants or convenience methods for properties (of a server)
	 * @author Alex
	 *
	 */
	public static final class properties {
		/**
		 * Default property values if not defined.
		 * @author Alex
		 *
		 */
		public static final class defaults {
			public static final String BROADCAST_PATH = "/";
			
			public static final String BROADCAST_ALLOWED_TYPES = "";

			public static final String PROGRAMS_VLC_PATH = "";
			
			public static final String PROGRAMS_OFFICE_PATH = "";
			
			public static final String PROGRAMS_PDFREADER_PATH = "";
			
			private defaults() {}
		}
		
		public static final String BROADCAST_PATH = "rc.broadcast.path";
		
		public static final String BROADCAST_ALLOWED_TYPES = "rc.broadcast.allowed_types";
		
		public static final String PROGRAMS_VLC_PATH = "rc.programs.vlc.path";
		
		public static final String PROGRAMS_OFFICE_PATH = "rc.programs.office.path";
		
		public static final String PROGRAMS_PDFREADER_PATH = "rc.programs.pdfreader.path";
		
		private properties() {}
	}
	
	public static final class objects {
		public static <T> T requireNonNull(T obj) throws NullPointerException {
			if(obj == null) {
				throw new NullPointerException();
			}
			
			return obj;
		}
	}
	
	/**
	 * Logger utility for logging.
	 */
	public static final Logger LOGGER = new JDKLogger();
	
	/**
	 * Google's Gson utility to parse to and from JSON.
	 */
	public static final Gson GSON = new Gson();
	
	private RC() {}
}
